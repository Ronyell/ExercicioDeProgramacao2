package Model;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Pgm extends Imagem {
    
    private int posicaoInicial;

    public Pgm() {
        super();
        this.posicaoInicial = 0;
    }

    public Pgm(String endereco) {
        super(endereco);
        this.posicaoInicial = 0;
    }

    public int getPosicaoInicial() {
        return posicaoInicial;
    }

    public void setPosicaoInicial(int posicaoInicial) {
        this.posicaoInicial = posicaoInicial;
    }

    public void pegarPosicaoInicial() throws IOException {
        abrirArquivo();
        int caracter = getArquivo().read();
        int contador = 0, contador1 = 0;
        String posicaoInicial = "";
        while (caracter != -1) {
            if ((char) caracter == '#') {
                getArquivo().read();
                caracter = getArquivo().read();
                while ((char) caracter != ' ' && (char) caracter != '\n') {
                    posicaoInicial += (char) caracter;
                    caracter = getArquivo().read();
                }
                setPosicaoInicial(Integer.parseInt(posicaoInicial));
                break;
            }
            contador++;
            caracter = getArquivo().read();
        }
        fecharArquivo();

    }

    @Override
    public BufferedImage pegarPixel() throws IOException {
        BufferedImage imagem_pgm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        int contador = 0, contador1=0;
        int caracter = getArquivo().read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador1--;
                }
            }
            if (contador1 == 0) {
                for(int contador2=0;contador2<getAltura()*getLargura();contador2++) {
                    pixels[contador2] = (byte) caracter;
                    contador++;
                    caracter = getArquivo().read();
                }
                break;
            }
            caracter = getArquivo().read();
        }        
        setPixel(((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData());
        setPixel(pixels);
        return imagem_pgm;
    }

    public String decifrarImagem() {
        int contador1 = 0;
        int pixel = 0, caracter = 0;
        String mensagem="";
        for (int contador = getPosicaoInicial() + 1; contador1 < 8; contador++) {
            caracter = pixel | (caracter << 1);
            pixel = ((int) getPixel()[contador]) & 0x001;
            if (contador1 == 7) {
                if ((char) caracter != '#') {
                    mensagem+=((char) caracter);
                }
                if (caracter != '#' || contador < getPosicaoInicial() + 16) {
                    contador1 = -1;
                    caracter = 0;
                }
            }
            contador1++;
        }
        return mensagem;
    }

    @Override
    public BufferedImage transformaNegativo() throws FileNotFoundException, IOException {
        BufferedImage imagempgm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels = ((DataBufferByte) imagempgm.getRaster().getDataBuffer()).getData();       
        
        for (int contador = 0; contador < getPixel().length; contador++) {
            pixels[contador]=(byte) (getValorMaximo() - getPixel()[contador]);
        }
        setPixelFiltro(pixels);
        return imagempgm;
    }

    public BufferedImage transformaSharpen() throws FileNotFoundException, IOException {
        int filtro[]={0, -1, 0, -1, 5, -1, 0, -1, 0};
        int value;
        BufferedImage imagem_pgm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels3 = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        char[] pixels = new char[getAltura()*getLargura()];
        abrirArquivo();
        setNumeroMagico("");
        pegarDados();
        int contador=0;
        int caracter = getArquivo().read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador--;
            }
            if ((char) caracter == '\n') {
                contador++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador--;
                }
            }
            if (contador == 0) {
                while (caracter != -1) {
                    pixels[contador] = (char) caracter;
                    contador++;
                    caracter = getArquivo().read();
                }
                break;
            }
            caracter = getArquivo().read();
        }
        fecharArquivo();
                     
      for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getLargura(); contador2++){
                        pixels3[contador1+contador2*getLargura()] = (byte)getPixel()[contador1+contador2*getLargura()];
		}
	}
	
	for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getAltura(); contador2++){
                        pixels3[contador1+contador2*getLargura()] = (byte)getPixel()[contador1+contador2*getLargura()];
		}
	}

	for(int contador1=3/2; contador1<getLargura()-3/2; contador1++){
		for(int contador2=3/2; contador2<getAltura()-3/2; contador2++){
			value = 0;
			for(int contador3=-3/2; contador3<=3/2; contador3++){
				for(int contador4=-3/2; contador4<=3/2;contador4++){
					value += filtro[(contador3+1)+3*(contador4+1)]*(pixels[(contador1+contador3)+(contador4+contador2)*getLargura()]);
				}
			}
				
				value/=1;
				value = value < 0 ? 0 : value;
				value = value > getValorMaximo()? getValorMaximo(): value;
                                pixels3[contador1+contador2*getLargura()] = (byte) value;
		}
	}
        setPixelFiltro(pixels3);
        return imagem_pgm;
    }
    
    public BufferedImage transformaSmooth() throws FileNotFoundException, IOException {
        int filtro[]={1, 1, 1, 1, 1, 1, 1, 1, 1};
        int value;
        BufferedImage imagem_pgm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels3 = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        char[] pixels = new char[getAltura()*getLargura()];
        abrirArquivo();
        setNumeroMagico("");
        pegarDados();
        int contador=0;
        int caracter = getArquivo().read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador--;
            }
            if ((char) caracter == '\n') {
                contador++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador--;
                }
            }
            if (contador == 0) {
                while (caracter != -1) {
                    pixels[contador] = (char) caracter;
                    contador++;
                    caracter = getArquivo().read();
                }
                break;
            }
            caracter = getArquivo().read();
        }
        fecharArquivo();
                     
      for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getLargura(); contador2++){
                        pixels3[contador1+contador2*getLargura()] = (byte)getPixel()[contador1+contador2*getLargura()];
		}
	}
	
	for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getAltura(); contador2++){
                        pixels3[contador1+contador2*getLargura()] = (byte)getPixel()[contador1+contador2*getLargura()];
		}
	}

	for(int contador1=3/2; contador1<getLargura()-3/2; contador1++){
		for(int contador2=3/2; contador2<getAltura()-3/2; contador2++){
			value = 0;
			for(int contador3=-3/2; contador3<=3/2; contador3++){
				for(int contador4=-3/2; contador4<=3/2;contador4++){
					value += filtro[(contador3+1)+3*(contador4+1)]*(pixels[(contador1+contador3)+(contador4+contador2)*getLargura()]);
				}
			}
				
				value/=9;
				value = value < 0 ? 0 : value;
				value = value > getValorMaximo()? getValorMaximo() : value;
                                pixels3[contador1+contador2*getLargura()] = (byte) value;
		}
	}
        setPixelFiltro(pixels3);
        return imagem_pgm;
    }

    
    
    public void salvarImagem(String endereco) throws FileNotFoundException, IOException{
        File file = new File(endereco + ".pgm");
        FileOutputStream temp;
        
            temp = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            temp.write(getNumeroMagico().getBytes());
            temp.write('\n');
            String comentario = "# "+getPosicaoInicial();
            temp.write(comentario.getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getAltura()).getBytes());
            temp.write(' ');
            temp.write(Integer.toString(getLargura()).getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getValorMaximo()).getBytes());
            temp.write('\n');
            temp.write(getPixelFiltro());
            temp.close();
      

    }
}
