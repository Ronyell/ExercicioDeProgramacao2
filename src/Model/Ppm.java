package Model;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;

public class Ppm extends Imagem {

    public Ppm() {
        super();
    }

    public Ppm(String endereco) {
        super(endereco);
    }

    public BufferedImage transformaRed() throws FileNotFoundException, IOException {
        BufferedImage imagemppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData();
        int contador1 = 0;
        for (int contador = 0; contador < getPixel().length; contador++) {
            if (contador1 == 2) {
                pixels[contador] = (getPixel()[contador]);
                contador1 = 0;
            } else {
                pixels[contador] = ((byte) 0);
                contador1++;
            }
        }
        setPixelFiltro(pixels);
        return imagemppm;
    }

    public BufferedImage transformaGreen() throws FileNotFoundException, IOException {
        BufferedImage imagemppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData();
        int contador1 = 0;
        for (int contador = 0; contador < getPixel().length; contador++) {
            if (contador1 == 1) {
                pixels[contador] = (getPixel()[contador]);
                contador1++;
            } else {
                pixels[contador] = ((byte) 0);
                contador1++;
                if (contador1 == 3) {
                    contador1 = 0;
                }
            }
        }
        setPixelFiltro(pixels);
        return imagemppm;
    }

    public BufferedImage transformaBlue() throws FileNotFoundException, IOException {
        BufferedImage imagemppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData();
        int contador1 = 0;
        for (int contador = 0; contador < getPixel().length; contador++) {
            if (contador1 == 0) {
                pixels[contador] = (getPixel()[contador]);
                contador1++;
            } else {
                pixels[contador] = ((byte) 0);
                contador1++;
                if (contador1 == 3) {
                    contador1 = 0;
                }
            }

        }
        setPixelFiltro(pixels);
        return imagemppm;
    }

    @Override
    public BufferedImage pegarPixel() throws IOException {
        BufferedImage imagemppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData();
        int contador = 0, contador1=0;
        int caracter = getArquivo().read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador1--;
                }
            }
            if (contador1 == 0) {
                for (int contador2=2;contador2<getAltura()*getLargura()*3;contador2+=3) {
                        pixels[contador2] = (byte) caracter;
                        contador++;
                        caracter = getArquivo().read();
                        pixels[contador2-1] = (byte) caracter;
                        contador++;
                        caracter = getArquivo().read();
                        pixels[contador2-2] = (byte) caracter;
                        contador++;                    
                        caracter = getArquivo().read();
                }
                break;
            }
            caracter = getArquivo().read();
        }
        setPixel(((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData());
        setPixel(pixels);
        setPixelFiltro(pixels);
        return imagemppm;
    }

    @Override
    public BufferedImage transformaNegativo() throws FileNotFoundException, IOException {
        BufferedImage imagemppm = new BufferedImage(getAltura(), getLargura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) imagemppm.getRaster().getDataBuffer()).getData();
        for (int contador = 0; contador < getPixel().length; contador++) {
            pixels[contador] = (byte) (getValorMaximo() - getPixel()[contador]);
        }
        setPixelFiltro(pixels);
        return imagemppm;
    }
    public void salvarImagem(String endereco) throws FileNotFoundException, IOException{
        File file = new File(endereco + ".ppm");
        FileOutputStream temp;
        
            temp = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            temp.write(getNumeroMagico().getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getAltura()).getBytes());
            temp.write(' ');
            temp.write(Integer.toString(getLargura()).getBytes());
            temp.write('\n');
            temp.write(Integer.toString(getValorMaximo()).getBytes());
            temp.write('\n');
            for(int contador = 2; contador<getPixelFiltro().length;contador+=3){
                temp.write(getPixelFiltro()[contador]);
                temp.write(getPixelFiltro()[contador-1]);
                temp.write(getPixelFiltro()[contador-2]);
            }
            temp.close();
      

    }
    
}
