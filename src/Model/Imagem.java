package Model;

import java.awt.image.BufferedImage;
import java.io.*;

public abstract class Imagem {

    private int largura;
    private int altura;
    private String numeroMagico;
    private int valorMaximo;
    private byte[] pixel;
    private FileInputStream arquivo;
    private String endereco;
    private int tamanhoTotal;
    private BufferedImage imagem;
    private byte [] pixelFiltro;

    
    public Imagem() {
        this.altura = 0;
        this.largura = 0;
        this.numeroMagico = "";
        this.valorMaximo = 0;
        this.endereco = "";

    }
    
    public Imagem(String endereco) {
        this.altura = 0;
        this.largura = 0;
        this.numeroMagico = "";
        this.valorMaximo = 0;
        this.endereco = endereco;

    }

    public FileInputStream getArquivo() {
        return arquivo;
    }

    public void setArquivo(FileInputStream arquivo) {
        this.arquivo = arquivo;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public String getNumeroMagico() {
        return numeroMagico;
    }

    public void setNumeroMagico(String numeroMagico) {
        this.numeroMagico = numeroMagico;
    }

    public int getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(int valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public byte[] getPixel() {
        return pixel;
    }

    public void setPixel(byte[] pixel) {
        this.pixel = pixel;
    }

    public int getTamanhoTotal() {
        return tamanhoTotal;
    }

    public void setTamanhoTotal(int tamanhoTotal) {
        this.tamanhoTotal = tamanhoTotal;
    }

    public void abrirArquivo() throws FileNotFoundException {
        setArquivo(new FileInputStream(getEndereco()));
    }

    public void fecharArquivo() throws IOException {
        getArquivo().close();
    }

    public void pegarDados() throws IOException {
        pegarNumeroMagico();
        pegarAltura();
        pegarLargura();
        pegarValorMaximo();
    }

    public void pegarNumeroMagico() throws IOException {
        int contador = 1, contador1 = 0;
        
        int caracter = getArquivo().read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
                contador++;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador1--;
                    contador++;
                }
            }
            if (contador1 == 0) {
                while (caracter != '\n') {
                    setNumeroMagico(getNumeroMagico() + (char) caracter);
                    caracter = getArquivo().read();
                    contador++;
                }
                break;
            }
            contador++;
            caracter = getArquivo().read();
        }
        setTamanhoTotal(contador);
    }

    public void pegarAltura() throws IOException {
        int contador = 1, contador1 = 0, caracter = getArquivo().read();
        String altura = "";
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                contador++;
                if ((char) caracter == '#') {
                    contador1--;
                    contador++;
                }
            }
            if (contador1 == 0) {
                while (caracter != ' ') {
                    altura += (char) caracter;
                    contador++;
                    caracter = getArquivo().read();
                }
                setAltura(Integer.parseInt(altura));
                break;
            }
            contador++;
            caracter = getArquivo().read();
        }
        setTamanhoTotal(getTamanhoTotal() + contador);
    }

    public void pegarLargura() throws IOException {
        int contador = 1, contador1 = 0, caracter = getArquivo().read();
        String largura = "";
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
                contador++;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                if ((char) caracter == '#') {
                    contador1--;
                    contador++;
                }
            }
            if (contador1 == 0) {
                while (caracter != '\n') {
                    largura += (char) caracter;
                    caracter = getArquivo().read();
                    contador++;
                }
                setLargura(Integer.parseInt(largura));
                break;
            }
            contador++;
            caracter = getArquivo().read();
        }
        setTamanhoTotal(getTamanhoTotal() + contador);
    }

    public void pegarValorMaximo() throws IOException {
        int contador = 1, contador1 = 0, caracter = getArquivo().read();
        String valorMaximo = "";
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = getArquivo().read();
                contador++;
                if ((char) caracter == '#') {
                    contador1--;
                    contador++;
                }
            }
            if (contador1 == 0) {
                while (caracter != '\n') {
                    valorMaximo += (char) caracter;
                    contador++;
                    caracter = getArquivo().read();
                }
                setValorMaximo(Integer.parseInt(valorMaximo));
                break;
            }
            contador++;
            caracter = getArquivo().read();
        }
        setTamanhoTotal(getTamanhoTotal() + contador);
    }

    public abstract BufferedImage pegarPixel() throws IOException;
    public abstract  BufferedImage transformaNegativo() throws FileNotFoundException, IOException;
    

    public BufferedImage getImagem() {
        return imagem;
    }
    
    public void setImagem(BufferedImage imagem) {
        this.imagem = imagem;
    }
    public static int testaNumeroMagico(String endereco) throws FileNotFoundException, IOException{
        int contador1 = 0;
        String numeroMagico="";
        FileInputStream arquivo = new FileInputStream(endereco);
        
        String extensao=endereco.substring(endereco.length()-3, endereco.length());
        int caracter = arquivo.read();
        while (caracter != -1) {
            if ((char) caracter == '#') {
                contador1--;
            }
            if ((char) caracter == '\n') {
                contador1++;
                caracter = arquivo.read();
                if ((char) caracter == '#') {
                    contador1--;
                }
            }
            if (contador1 == 0) {
                while (caracter != '\n') {
                    numeroMagico+= (char) caracter;
                    caracter = arquivo.read();
                }
                break;
            }
            caracter = arquivo.read();
        }
        arquivo.close();
        if(numeroMagico.equals("P5") &&(extensao.equals("pgm"))){
            return 1;
        }
        else if(numeroMagico.equals("P6") &&(extensao.equals("ppm"))){
            return 2;
        }
        else {        
            throw new IllegalArgumentException("Arquivo não é Pgm ou Ppm ou está corrompido.");
        }
    }    
    
    
    public byte[] getPixelFiltro() {
        return pixelFiltro;
    }

    /**
     * @param pixelFiltro the pixelFiltro to set
     */
    public void setPixelFiltro(byte[] pixelFiltro) {
        this.pixelFiltro = pixelFiltro;
    }
}
