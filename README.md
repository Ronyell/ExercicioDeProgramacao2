<center> <b><font color = "red">README</font></b></center>
<p>src: Pasta que contém pacotes com arquivos .java.</p>

<p>O programa foi feito em java com um ambiente de desenvolvimeto (IDE) netbeans.</p>
<p>Para execução do programa é necessário que haja a JVM, JRE. </p>

<center> <b><font color = "blue">Passo a passo</font></b></center>

<p><font color = "#0000FF">Primeiro</font> - Clique no botão abrir imagem e selecione uma imagem.</p>
<p><font color = "blue">Segundo</font> - Abrirá uma outra tela que depende do formato do arquivo especificado, PGM ou PPM.</p>
<p><font color = "blue">Terceiro</font> - Selecione um dos filtros ou, no caso da imagem PGM, decifrar imagem e clique em aplicar, será aplicado a imagem o filtro ou abrirá uma outra janela com a mensagem.</p>
<p><font color = "blue">Quarto</font> - Caso aplique algum dos filtros o botão salvar poderá ser pressionado, caso queira salvar, salve.</p>
<p><font color = "blue">Quinto</font> - Repita o processo.</p>

<p><b><font color = "blue">Observações</font></b></p>
<p>O programa contém uma interface onde só é possível abrir arquivos do tipo PGM ou PPM, caso o arquivo especificado 
não seja um dos dois a aplicação informará o que o arquivo não é PGM ou PPM.</p> 

